import Vue from "vue";
import App from "./App.vue";
import VueCarousel from 'vue-carousel';
import  store  from './store/index'
import BootstrapVue from 'bootstrap-vue'

Vue.use(BootstrapVue);

// app.js
    import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(VueCarousel);

Vue.config.productionTip = false;
new Vue({
  el: '#app',
  render: h => h(App),
  store: store,
  components: { App },
  template: '<App/>'
})

