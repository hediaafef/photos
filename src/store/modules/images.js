const axios = require('axios');

/* eslint-disable */
const state = {
  images: [],
};

const actions = {

  async fetchAllCurrentImages({ commit }) {

// Make a request for a user with a given ID
    axios.get('https://jsonplaceholder.typicode.com/photos')
        .then(function (response) {

          console.log(response);

          commit('setImagesList', response.data.splice(0,100));
        })
        .catch(function (error) {
          // handle error
          console.log(error);
        })
        .finally(function () {
          // always executed
        });



  },
};

const mutations = {

  setImagesList(state, images) {
   state.images=images;
  },
};

const getters = {
  getImagesList: state => state.images,

};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
