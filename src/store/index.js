import Vue from 'vue';
import Vuex from 'vuex';
import imagesModule from './modules/images';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    images: imagesModule,
  },
});
